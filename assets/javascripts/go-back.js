$(document).ready(function () {
  $('.content .log.title').on('click', function () {
    var url = $(location).attr('href').split('#')[0];
    var position = $(this).attr('position');
    sessionStorage.gitee_log_parent_url = url + '#' + position;
  });

  $('.icon.go-back').parent().attr('href', sessionStorage.gitee_log_parent_url);
});
