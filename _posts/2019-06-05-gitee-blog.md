---
layout: post
title: "坑爹代码 | 为了后期优化查询速度 ~ 颇有商业头脑！"
---

<p>什么样的程序员是一个好程序员呢？当我们在给客户开发系统时候，为了后期的优化，预留一些埋点。</p>

<p>通过对这些埋点的优化，可以让客户瞬间感觉系统在运行速度上有质的飞跃，让公司顺利的签署二期开发合同，收取巨额开发费用。</p>

<p>从公司角度来看，这样的程序员就是一个好程序员。 &mdash;&mdash; 这句话不是红薯说的！</p>

<p><img alt="" height="640" src="https://static.oschina.net/uploads/space/2019/0605/073407_Gr1W_12.jpg" width="538" /></p>

<p>比如：</p>

<pre>
<code class="language-java">/***
 * 为了后期优化查询速度
 */
public class Sleep {
	
	public Object getObect(){
		/***
		  查询代码
		*/
		Thread.sleep(5000)
		return object;
	}

}</code></pre>

<p>我想说的是：凶碟，你下手能否不那么狠啊，建议对代码进行优化，改成 <strong>Thread.sleep(1000);&nbsp;&nbsp;</strong>&mdash;&mdash; 这句话也不是红薯说的。</p>

<p>或者你有什么更好的建议呢？不要觉得骇人听闻，真有这样的人，这样的代码！！！</p>

<p>请到下面链接发表评论，领取奖品：</p>

<p><a href="https://gitee.com/oschina/bullshit-codes/blob/master/java/Sleep.java">https://gitee.com/oschina/bullshit-codes/blob/master/java/Sleep.java</a></p>

<p>另外一种隐性的坑请看：</p>

<p><a href="https://gitee.com/oschina/bullshit-codes/blob/master/php/%E5%85%A5%E8%81%8C%E7%AC%AC%E4%B8%80%E5%A4%A9.php">入职第一天.php</a></p>

<p>码云 6 周年，我们正在征集各种坑爹代码，很多奖品等你来拿</p>

<p>详细的参与方法请看&nbsp;&nbsp;<a href="https://gitee.com/oschina/bullshit-codes">https://gitee.com/oschina/bullshit-codes</a></p>

<p>------ 分割线 ------</p>

<p>其他坑爹代码吐槽：</p>

<ul>
	<li><a href="https://www.oschina.net/news/107032/gitee-bullshit-codes-stringbuffer" target="_blank">坑爹代码&nbsp;| 这样使用 StringBuffer 的方法有什么坑？</a></li>
	<li><a href="https://www.oschina.net/news/107081/gitee-bad-code-one-line-code" target="_blank">坑爹代码&nbsp;| 你写过的最长的一行代码有多长？？？</a></li>
	<li><a href="https://www.oschina.net/news/107151/nesting-bad-code">坑爹代码 | 循环+条件判断，你最多能嵌套几层？</a></li>
</ul>
