---
layout: post
title: "Gitee 现已增加 WebHook 对企业微信的支持"
---

<p><span style="color:null"><span style="background-color:#ffffff">为了方便用户在第三方平台接收到 Gitee 的推送，Gitee 推出了 Webhook 功能，此前你已经可以通过 Webhook 和你的钉钉机器人与 Slack 机器人进行连接，接收推送。</span></span></p>

<p><span style="color:null">为了满足更多平台用户接收推送的需求， <strong>Gitee 现已增加 Webhook 对企业微信的支持。</strong></span></p>

<p><span style="color:null"><span style="background-color:#ffffff">企业微信在内部群聊中提供了「群机器人」功能。通过 WebHook，可以在企业微信中添加自定义的机器人实现自动通知。</span></span></p>

<h3 style="text-align:start">添加机器人</h3>

<p style="text-align:start"><span style="color:null">在企业微信群聊中，通过聊天窗口在内部群右键菜单选择&nbsp;<code>添加群机器人</code>-&gt;<code>新创建一个机器人</code>，为机器人设置一个头像和名称，点击「添加」后可以获得一个 WebHook 地址，关闭窗口即可完成企业微信内部群微信机器人的添加。</span></p>

<blockquote>
<p>目前包含外部联系人的群聊不支持添加机器人。</p>
</blockquote>

<p><img alt="" height="398" src="https://images.gitee.com/uploads/images/2020/0327/092629_5890bc35_551147.png" width="304" /></p>

<p><img alt="" height="164" src="https://images.gitee.com/uploads/images/2020/0327/092810_d7943138_551147.png" width="600" /></p>

<p><img alt="" height="330" src="https://images.gitee.com/uploads/images/2020/0327/092827_ce2ff74a_551147.png" width="480" /></p>

<p><img alt="" height="330" src="https://images.gitee.com/uploads/images/2020/0327/093031_727106a2_551147.png" width="480" /></p>

<h3 style="text-align:start">设置机器人 WebHook</h3>

<p style="text-align:start">在上文的添加过程中，可以获得一个形如&nbsp;<code>https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx</code>&nbsp;的 WebHook 请求地址，将这个地址添加到 Gitee 上，即可完成对 WebHook 的设置。</p>

<p style="text-align:start">在 Gitee 仓库页面，通过「管理」-&gt;「WebHooks设置」-&gt;「添加」，可以添加一个新的 WebHook。</p>

<p style="text-align:start">将上文得到的 WebHook 的地址填写到 URL 中，选择具体的触发事件，激活并添加即可完成对 WebHook 的设置。</p>

<p style="text-align:start"><img alt="" height="462" src="https://images.gitee.com/uploads/images/2020/0327/093313_1f59cac5_551147.png" width="700" /></p>

<h3 style="text-align:start">IP 白名单</h3>

<p style="text-align:start">通过设置请求 IP 地址（段）, 可以限制触发机器人的请求 IP 白名单。由于 Gitee 服务请求所在的 IP 段不是固定的，<strong>为避免无法正常触发机器人，不建议设置 IP 地址（段）白名单。</strong></p>

<p style="text-align:start"><img alt="" height="344" src="https://images.gitee.com/uploads/images/2020/0327/093557_a03e2928_551147.png" width="680" /></p>

<h3 style="text-align:start">触发 WebHooks</h3>

<p style="text-align:start">通过设置机器人，以下场景会触发 WebHook 请求到企业微信。</p>

<ul>
	<li>Push: 仓库推送代码，推送分支，删除分支</li>
	<li>Tag push: 新建 tag，删除 tag</li>
	<li>Issue ： 新建、关闭、重新打开、删除任务 或 修改任务指派人</li>
	<li>Pull request : 新建pull request、更新pull request、合并 pull request</li>
	<li>评论: 评论仓库、issue（任务）、pull request、commit</li>
</ul>
