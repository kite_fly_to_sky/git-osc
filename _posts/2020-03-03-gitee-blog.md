---
layout: post
title: "维护通知：Gitee 系统将于 3 月 5 日凌晨升级扩容"
---

<p>Gitee 将于 2020 年 3 月 5 日（本周四）凌晨 01:00～01:30 对系统进行升级维护，扩容核心数据库。</p>

<p>届时 Gitee 服务将中断、无法访问，请提前做好安排，敬请谅解。</p>

<p>Gitee：<a href="https://gitee.com/">https://gitee.com</a></p>

<p><img height="341" src="https://static.oschina.net/uploads/space/2020/0303/153547_IxRK_3820517.png" width="700" /></p>
