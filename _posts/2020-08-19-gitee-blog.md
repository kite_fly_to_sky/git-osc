---
layout: post
title: "Gitee 上线多项 PR 功能优化，进一步提升审查与提交效率"
---

<p>无论在开源项目贡献中还是研发团队的协作中，Pull Request 都是非常关键的一环，Gitee 也一直在努力优化 Pull Request 的使用体验。</p>

<p>此前 Gitee 已经陆续上线了<strong><a href="https://www.oschina.net/news/114042/gitee-release-lightweight-pull-requests" target="_blank"><span style="color:#000000">「轻量级 Pull Request」</span></a></strong>以及<strong><a href="https://www.oschina.net/news/116686/gitee-pull-request" target="_blank"><span style="color:#000000">「Pull Request 代码已阅」</span></a></strong>功能，近期我们再次对 Gitee 的 Pull Request 模块进行了如下的升级：</p>

<h4>&nbsp;Pull Request 代码多行评论</h4>

<p>当审查者对一条 PR 进行审查时，可以使用评论功能对代码提出修改意见或发表看法，但想要进行多行代码的评论的话就变得很麻烦。</p>

<p>Gitee 现在已经支持审查者可以&nbsp;通过拖拽的方式选中多行代码进行评论，信息更全面，沟通更清晰。&nbsp;</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-415f601c3e2f1f02f0fec5da522231d5e2e.gif" width="700" /></p>

<h4>评论支持显示代码上下文</h4>

<p>以前在 PR 中遇到对代码的评论时，需要点击评论下方的「链接地址」才能看到对应的代码，体验并不是很好。</p>

<p><img alt="" height="234" src="https://oscimg.oschina.net/oscnet/up-03e47dd67e735bacb72f2ed639fb06e079f.JPEG" width="700" /></p>

<p>现在 Gitee 已经支持了对代码的评论支持&nbsp;显示当时评论的代码内容及上下文&nbsp;，当对应评论的代码行被更新或对历史代码进行评论，评论都将自动标记为过期状态。&nbsp;</p>

<h4>提交 PR 时自动指定审查者</h4>

<p>以前在 Gitee 提交 Pull Request 时，需要创建 PR 的开发者手动指派 PR 审查 / 测试人员，这不论是在研发团队协作以及开源项目协作中都是极其不便的。</p>

<p>现在 Gitee 支持仓库管理员&nbsp;设置默认的 PR（审查/测试）指派名单&nbsp;，开发者提交新 PR 时，会自动根据设置指定审查者且不可更改。&nbsp; &nbsp; &nbsp;</p>

<p><img alt="" height="444" src="https://oscimg.oschina.net/oscnet/up-f7a021f5c58d97990190c44371aeef557ce.png" width="700" /></p>

<p><img alt="" height="399" src="https://oscimg.oschina.net/oscnet/up-6104be99cc4eaf4eafaa3f5d8be43807ebe.png" width="700" /></p>

<h4>支持查看历史代码 Diff 差异</h4>

<p>上面的更新中提到了 PR 的评论支持过期状态，以便查看历史 PR 的评论，那么历史代码当然也要跟上。</p>

<p>本次更新后 Gitee 也支持了&nbsp;查看历史代码 Diff 差异&nbsp;的功能。帮助团队清晰的了解代码在每个版本中的更改历史及每次更改的差异，更快更方便的回顾代码，从而发现可能的问题隐患。</p>

<p><img alt="" src="https://oscimg.oschina.net/oscnet/up-631a02aedbf4a26028c56e555f268ef68f6.gif" width="700" /></p>

<p>如果你对目前 Gitee 的&nbsp;Pull Request 使用有任何的意见和建议，欢迎评论告诉我们，Gitee 在提升产品使用体验的路上需要更多来自可爱的你们的声音~</p>
